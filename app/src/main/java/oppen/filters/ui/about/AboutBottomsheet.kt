package oppen.filters.ui.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottomsheet_about.view.*
import oppen.filters.R

class AboutBottomsheet: BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottomsheet_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        //OppenLab Header
        view.codeberg_button.setOnClickListener {
            openBrowser("https://codeberg.org/oppen/Latt")
        }

        view.mastodon_button.setOnClickListener {
            openBrowser("https://fosstodon.org/@oppen")
        }

        view.nounproject_button.setOnClickListener {
            openBrowser("https://thenounproject.com/")
        }

        view.raw_therapee_button.setOnClickListener {
            openBrowser("https://rawpedia.rawtherapee.com/Film_Simulation")
        }

        //OppenLab License/footer
        view.gnu_license_button.setOnClickListener {
            openBrowser("https://www.gnu.org/licenses/gpl-3.0.html")
        }

        view.oppenlab_button.setOnClickListener {
            openBrowser("https://oppenlab.net")
        }
    }

    private fun openBrowser(url: String){
        startActivity(Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(url)
        })
    }
}