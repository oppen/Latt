package oppen.filters.ui.filter_picker

/*

    Lätt - fast image filtering app for Android
    Copyright (C) 2020  Öppenlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

import android.content.Context
import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottomsheet_filters.view.*
import oppen.filters.R
import oppen.filters.io.Luts

class FilterPickerDialog(
    context: Context,
    thumbnailUri: Uri,
    luts: List<Pair<String, Int>>,
    private val isUserSavedFilters: Boolean,
    private val onFilterSelect: (lut: Pair<String, Int>) -> Unit): BottomSheetDialogFragment() {

    private val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    private val lazyLUTAdapter: LazyLUTAdapter = LazyLUTAdapter(context, thumbnailUri, luts.toMutableList(), { lut ->
        //Long click...
        val actions = arrayOf("Add to saved")
        if(isUserSavedFilters) actions[0] = "Remove from saved"
        AlertDialog.Builder(context)
            .setTitle(lut.first)
            .setItems(actions){ _, choice ->
                when (choice) {
                    0 -> {
                        if(isUserSavedFilters) {
                            Luts.removeSaved(context, lut.second)
                            removeLut(lut.second)
                        }else{
                            Luts.addSaved(context, lut.second)
                        }
                    }
                }
            }
            .show()
    }){ lut, direction, count ->
        onFilterSelect(lut)
        if (layoutManager.findLastCompletelyVisibleItemPosition() < count) {
            var position = layoutManager.findLastCompletelyVisibleItemPosition()
            if(direction == 1){
                position += 1
            }else if(direction == -1){
                position -= 3
            }

            if(position != RecyclerView.NO_POSITION) {
                layoutManager.smoothScrollToPosition(view?.thumbnail_recycler, null, position)
            }
        }
    }

    private fun removeLut(resId: Int) = lazyLUTAdapter.removeLut(resId)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottomsheet_filters, container, false)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        view?.thumbnail_recycler?.layoutManager = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.thumbnail_recycler.addItemDecoration(
            FiltersItemDecoration(
                resources.getDimensionPixelSize(R.dimen.thumbnail_padding)
            )
        )
        view.thumbnail_recycler.layoutManager = layoutManager
        view.thumbnail_recycler.adapter = lazyLUTAdapter
    }

    override fun onStart() {
        super.onStart()
        val window: Window? = dialog!!.window
        val windowParams: WindowManager.LayoutParams? = window?.attributes
        if(windowParams != null) {
            windowParams.dimAmount = 0.0f
            windowParams.flags = windowParams.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
            window.attributes = windowParams
        }
    }

}