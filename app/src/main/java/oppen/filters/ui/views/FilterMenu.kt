package oppen.filters.ui.views

import android.content.Context
import android.view.View
import androidx.appcompat.widget.PopupMenu
import oppen.filters.R
import oppen.filters.io.Luts


object FilterMenu {

    fun show(context: Context, anchor: View, onLutsReady: (luts: List<Pair<String, Int>>, isUserSavedFilters: Boolean) -> Unit){
        val popup = PopupMenu(context, anchor)
        popup.menuInflater.inflate(R.menu.filter_menu, popup.menu)

        popup.setOnMenuItemClickListener { item ->
            val resName = context.resources.getResourceEntryName(item.itemId)
            when {
                item.itemId == R.id.saved -> {
                    val luts = Luts.getSavedLuts(context)

                    if(luts == null){
                        //No saved luts
                        //todo - show dialog with instructions to save
                    }else {
                        onLutsReady(luts, true)
                    }
                    true
                }
                resName.startsWith("tl_") -> {
                    //Top level, let the system handle
                    false
                }
                else -> {
                    val luts = Luts.getLuts(context, item.itemId)
                    onLutsReady(luts, false)

                    true
                }
            }
        }

        popup.show()
    }
}