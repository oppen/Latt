package oppen.filters.io

/*

    Lätt - fast image filtering app for Android
    Copyright (C) 2020  Öppenlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.res.TypedArray
import android.util.Log
import oppen.filters.R
import oppen.filters.toLabel

// http://cullenkelly.com/blog/2017/8/17/low-contrast-look
// https://noamkroll.com/why-the-milky-black-look-is-now-the-most-overused-technique-in-amateur-cinematography/
object Luts {

    fun addSaved(context: Context, lutResId: Int){
        val prefs = context.getSharedPreferences("OPPENLUTS", MODE_PRIVATE)
        val savedLuts = prefs.getString("SAVEDLUTS", "")
        if(savedLuts!!.contains("$lutResId")) return
        if(savedLuts.isNullOrEmpty()){
            prefs.edit().putString("SAVEDLUTS", "$lutResId").apply()
        }else{
            prefs.edit().putString("SAVEDLUTS", "$savedLuts,$lutResId").apply()
        }

    }

    fun removeSaved(context: Context, lutResId: Int) {
        val prefs = context.getSharedPreferences("OPPENLUTS", MODE_PRIVATE)
        val savedLuts = prefs.getString("SAVEDLUTS", "")
        if(savedLuts!!.contains("$lutResId")){
            var removedLuts = savedLuts.replace("$lutResId", "")
            if(removedLuts.contains(",,")){
                removedLuts = removedLuts.replace(",,", ",")
            }
            prefs.edit().putString("SAVEDLUTS", removedLuts).apply()
        }
    }

    private fun getSavedLutIds(context: Context): IntArray?{
        val prefs = context.getSharedPreferences("OPPENLUTS", MODE_PRIVATE)
        val savedLuts = prefs.getString("SAVEDLUTS", "")
        return when {
            savedLuts.isNullOrEmpty() -> null
            else -> {
                Log.d("LUTS", "Saved LUTS string: $savedLuts")
                val resIds = savedLuts.split(",").map { it.toInt() }
                resIds.toIntArray()
            }
        }
    }

    fun getSavedLuts(context: Context): List<Pair<String, Int>>?{
        val resIds = getSavedLutIds(context) ?: return null

        val luts = arrayListOf<Pair<String, Int>>()

        resIds.forEach { resId ->
            if(resId != -1){
                val lutResName = context.resources.getResourceEntryName(resId)
                val label = lutResName!!.toLabel()
                luts.add(Pair(label, resId))
            }
        }

        return luts
    }

    fun getLuts(context: Context, menuId: Int): List<Pair<String, Int>>{

        val resName = context.resources.getResourceEntryName(menuId)
        require(!resName.startsWith("tl_")) { "Can't get resources for top level filter category" }

        val arrayKey = resName.substring(resName.indexOf("_") + 1, resName.length)
        val arrayId = context.resources.getIdentifier(arrayKey, "array", context.packageName)

        require(arrayId != 0) { "Can't get identifier for $arrayKey" }

        val lutIds: TypedArray = context.resources.obtainTypedArray(arrayId)

        val luts = arrayListOf<Pair<String, Int>>()

        for(index in 0 until lutIds.length()){
            val lutResId = lutIds.getResourceId(index, -1)
            if(lutResId != -1){
                val lutResName = context.resources.getResourceEntryName(lutResId)
                val label = lutResName!!.toLabel()
                luts.add(Pair(label, lutResId))
            }
        }

        lutIds.recycle()

        return luts
    }
}